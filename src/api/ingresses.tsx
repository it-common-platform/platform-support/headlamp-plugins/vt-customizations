import { ApiProxy } from "@kinvolk/headlamp-plugin/lib";

export function listIngresses(namespace: string): Promise<any> {
  return ApiProxy.request(
    `/apis/networking.k8s.io/v1/namespaces/${namespace}/ingresses`
  );
}
