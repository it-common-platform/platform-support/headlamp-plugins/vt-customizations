import {
  DefaultDetailsViewSection,
  registerDetailsViewSectionsProcessor,
  registerSidebarEntry,
} from "@kinvolk/headlamp-plugin/lib";
import {
  SectionBox,
  SimpleTable,
} from "@kinvolk/headlamp-plugin/lib/CommonComponents";
import { KubeObject } from "@kinvolk/headlamp-plugin/lib/k8s/cluster";
import { useEffect, useState } from "react";
import { listIngresses } from "../api/ingresses";

export interface IngressListProps {
  resource?: KubeObject;
}

export default function applyIngressCustomizations() {
  registerSidebarEntry({
    parent: "cluster",
    name: "ingresses",
    label: "Ingresses",
    url: "/ingresses",
    icon: "mdi:input",
  });

  registerDetailsViewSectionsProcessor();
}

function IngressList(props: IngressListProps) {
  const { resource } = props;
  const [ingresses, setIngresses] = useState<Array<any> | null>(null);

  const namespace = resource?.jsonData?.metadata?.name ?? "";

  useEffect(() => {
    if (!namespace) {
      // If namespace is not available, return early
      setIngresses([]);
      return;
    }

    listIngresses(namespace).then((response) => {
      if (!response?.items) {
        setIngresses([]);
        return;
      }
      setIngresses(response?.items);
    });
  }, []);

  return (
    <SectionBox title="Ingresses" textAlign="center" paddingTop={2}>
      <SimpleTable
        columns={[
          {
            label: "Domain and Path",
            getter: (ingress) =>
              ingress?.spec?.rules[0]?.host ??
              "" + ingress?.spec?.rules[0]?.http?.paths[0]?.path ??
              "",
          },
          {
            label: "Backend Port",
            getter: (ingress) =>
              ingress?.spec?.rules[0]?.http?.paths[0]?.backend?.service?.port
                ?.number ?? "",
          },
          {
            label: "Created",
            getter: (ingress) => ingress?.metadata?.creationTimestamp ?? "",
          },
        ]}
        data={ingresses}
      />
    </SectionBox>
  );
}

registerDetailsViewSectionsProcessor(function addSubheaderSection(
  resource,
  sections
) {
  // Ignore if there is no resource.
  if (!resource) {
    return sections;
  }

  if (resource.kind !== "Namespace") {
    // Return early if we're not on a namespace page
    return sections;
  }

  // Check if we already have added our custom section (this function may be called multiple times).
  const customSectionId = "add-ingresses-to-namespace";
  if (sections.findIndex((section) => section.id === customSectionId) !== -1) {
    return sections;
  }

  const detailsHeaderIdx = sections.findIndex(
    (section) => section.id === DefaultDetailsViewSection.MAIN_HEADER
  );
  // There is no header, so we do nothing.
  if (detailsHeaderIdx === -1) {
    return sections;
  }

  // We place our custom section after the header.
  sections.splice(detailsHeaderIdx + 8, 0, {
    id: "add-ingresses-to-namespace",
    section: <IngressList resource={resource} />,
  });

  return sections;
});
