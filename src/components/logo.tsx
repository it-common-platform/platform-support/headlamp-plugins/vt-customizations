import { Typography } from "@mui/material";

/**
 * A simple logo using two different SVG files.
 * One for the small logo (used in mobile view), and a larger one used in desktop view.
 *
 * The main benefit of a SVG logo is that
 * it's easier to make it look good with light and dark themes.
 */
export default function SimpleLogo() {
  return (
    <Typography variant="h5" component="span">
      VT Platform Dashboard
    </Typography>
  );
}
