import {
  registerRoute,
  registerSidebarEntry,
} from "@kinvolk/headlamp-plugin/lib";
import {
  PercentageBar,
  SectionBox,
  SimpleTable,
} from "@kinvolk/headlamp-plugin/lib/CommonComponents";
import { KubeObject } from "@kinvolk/headlamp-plugin/lib/k8s/cluster";
import {
  getPercentStr,
  getResourceStr,
} from "@kinvolk/headlamp-plugin/lib/Utils";
import Typography from "@mui/material/Typography";
import { useEffect, useState } from "react";
import { listNodepools } from "../api/nodepools";
import { parseRam } from "../utils/units";
import { Box } from "@mui/material";

export interface NodepoolListProps {
  resource?: KubeObject;
}

export interface ChartDataPoint {
  name: string;
  value: number;
  fill?: string;
}

export default function applyNodepoolCustomizations() {
  let url = window.location.href;
  if (url.startsWith("file://")) {
    // Using Headlamp desktop application
    url = "https://headlamp.dvlp.aws.itcp.cloud.vt.edu/c/main";
  }
  const baseUrl = url?.split("/")[2] ?? "";
  const location = baseUrl.split(".")[2].replace("op", "eksa") ?? "undefined";

  if (location === "aws") {
    // This is only for Karpenter nodepools for now
    registerSidebarEntry({
      parent: "cluster",
      name: "nodepools",
      label: "Nodepools",
      url: "/nodepools",
      icon: "mdi:server",
    });

    registerRoute({
      path: "/nodepools",
      sidebar: "Node Pools",
      name: "Node Pools",
      exact: true,
      component: () => <NodepoolList />,
    });
  }
}

function NodepoolList() {
  const [nodepools, setNodepools] = useState<Array<any> | null>(null);

  useEffect(() => {
    listNodepools().then((response) => {
      if (!response?.items) {
        setNodepools([]);
        return;
      }
      setNodepools(response?.items);
    });
  }, []);

  return (
    <SectionBox title="Nodepools" textAlign="center" paddingTop={2}>
      <SimpleTable
        columns={[
          {
            label: "Name",
            getter: (nodepool) => nodepool.metadata?.name ?? "",
            gridTemplate: 0.5,
          },
          {
            label: "CPU",
            getter: (nodepool) => {
              const used: ChartDataPoint[] = [
                {
                  name: "CPU",
                  value: nodepool.status?.resources?.cpu ?? 0,
                },
              ];

              const usedValue = nodepool.status?.resources?.cpu ?? 0;
              const limit = nodepool.spec?.limits?.cpu ?? 0;

              function tooltipFunc() {
                return (
                  <Typography>
                    {usedValue} of {limit} ({getPercentStr(usedValue, limit)})
                  </Typography>
                );
              }
              return (
                <div>
                  <Box sx={{ paddingBottom: "16px" }}>
                    <PercentageBar
                      data={used}
                      total={limit}
                      tooltipFunc={tooltipFunc}
                    />
                  </Box>
                </div>
              );
            },
          },
          {
            label: "Memory",
            getter: (nodepool) => {
              const usedValue = nodepool.status?.resources?.memory ?? "0";
              const limit = nodepool.spec?.limits?.memory ?? "0";
              const parsedUsedValue = parseRam(usedValue);
              const parsedLimit = parseRam(limit);

              const used: ChartDataPoint[] = [
                {
                  name: "Memory",
                  value: parsedUsedValue,
                },
              ];

              function tooltipFunc() {
                return (
                  <Typography>
                    {getResourceStr(parsedUsedValue, "memory")} of{" "}
                    {getResourceStr(parsedLimit, "memory")} (
                    {getPercentStr(parsedUsedValue, parsedLimit)})
                  </Typography>
                );
              }
              return (
                <div>
                  <Box sx={{ paddingBottom: "16px" }}>
                    <PercentageBar
                      data={used}
                      total={parsedLimit}
                      tooltipFunc={tooltipFunc}
                    />
                  </Box>
                </div>
              );
            },
          },
          {
            label: "Ephemeral Storage",
            getter: (nodepool) => {
              const resources = nodepool.status?.resources;
              let storageString: string;
              if (resources) {
                const ephemeralStorage = resources["ephemeral-storage"] ?? "0";
                const parsedEphemeralStorage = parseRam(ephemeralStorage) ?? 0;
                storageString =
                  getResourceStr(parsedEphemeralStorage, "memory") ?? "0";
              }
              return storageString;
            },
            gridTemplate: 0.5,
          },
          {
            label: "Pods",
            getter: (nodepool) => nodepool.status?.resources?.pods ?? "",
            gridTemplate: 0.5,
          },
        ]}
        data={nodepools}
      />
    </SectionBox>
  );
}
