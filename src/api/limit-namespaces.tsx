import { ApiProxy } from "@kinvolk/headlamp-plugin/lib";

export function listNamespaces(): Promise<{ items: any[] }> {
  return ApiProxy.request(`/api/v1/namespaces`);
}

export async function getNamespace(
  namespace: string
): Promise<{ items: any[] }> {
  return ApiProxy.request(`/api/v1/namespaces/${namespace}/status`);
}
