# vt-customizations

This Headlamp plugin includes all of the customizations specific to the VT Common Platform.
- Changes the Headlamp default logo
- Adds a help button to the upper right corner that links to Platform Docs
- Adds a cluster chooser that has a statically defined list of all of our clusters
- Adds a `Containers` section to the namespace details page that includes images and mounts
- Adds a `Nodepools` link under the `Cluster` sidebar item that shows Karpenter nodepools
- Adds an `Ingresses` link under the `Cluster` sidebar item and an `Ingresses` section in the namespace details page
- Adds Grafana links to the list pages for namespaces, nodes, and pods.
- Adds Splunk links to the list pages for namespaces, nodes, pods, statefulsets, daemonsets, and replicasets.

## Developing Headlamp plugins

For more information on developing Headlamp plugins, please refer to:

- [Platform Onboarding](https://onboarding.platform.it.vt.edu/development-environment/headlamp-plugins/)
- [Getting Started](https://headlamp.dev/docs/latest/development/plugins/), How to create a new Headlamp plugin.
- [API Reference](https://headlamp.dev/docs/latest/development/api/), API documentation for what you can do
- [UI Component Storybook](https://headlamp.dev/docs/latest/development/frontend/#storybook), pre-existing components you can use when creating your plugin.
- [Plugin Examples](https://github.com/headlamp-k8s/headlamp/tree/main/plugins/examples), Example plugins you can look at to see how it's done.
