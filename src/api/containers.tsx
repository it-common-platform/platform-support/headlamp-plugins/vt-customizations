import { ApiProxy } from "@kinvolk/headlamp-plugin/lib";

export function listPods(namespace: string): Promise<any> {
  return ApiProxy.request(`/api/v1/namespaces/${namespace}/pods`);
}
