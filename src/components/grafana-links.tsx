import { Icon } from "@iconify/react";
import { registerResourceTableColumnsProcessor } from "@kinvolk/headlamp-plugin/lib";
import Namespace from "@kinvolk/headlamp-plugin/lib/k8s/namespace";
import Node from "@kinvolk/headlamp-plugin/lib/k8s/node";
import Pod from "@kinvolk/headlamp-plugin/lib/k8s/pod";
import Ingress from "@kinvolk/headlamp-plugin/lib/lib/k8s/ingress";
import Service from "@kinvolk/headlamp-plugin/lib/lib/k8s/service";
import { IconButton } from "@mui/material";

export default function applyGrafanaLinks() {
  let url = "https://headlamp.dvlp.test.itcp.cloud.vt.edu/c/main";
  if (window.location.href.startsWith("https://")) {
    url = window.location.href;
  }

  const baseUrl = url?.split("/")[2] ?? ""; // Example baseUrl: headlamp.dvlp.aws.itcp.cloud.vt.edu
  const grafanaDashboardsUrl = `https://${baseUrl.replace(
    "headlamp",
    "grafana"
  )}/d`;
  const podDashboardId =
    "6581e46e4e5c7ba40a07646395ef7b23/kubernetes-compute-resources-pod";
  const namespaceDashboardId =
    "85a562078cdf77779eaa1add43ccec1e/kubernetes-compute-resources-namespace-pods";
  const nodeDashboardId =
    "200ac8fdbfbb74b39aff88118e4d1c2c/kubernetes-compute-resources-node-pods";
  const options = "?orgId=1&refresh=10s&var-datasource=default&var-cluster=";
  const traefikDashboardId = "n5bu_kv4k/traefik-official-kubernetes-dashboard";

  const podDashboardUrl = `${grafanaDashboardsUrl}/${podDashboardId}${options}`;
  const namespaceDashboardUrl = `${grafanaDashboardsUrl}/${namespaceDashboardId}${options}`;
  const nodeDashboardUrl = `${grafanaDashboardsUrl}/${nodeDashboardId}${options}`;
  const traefikDashboardUrl = `${grafanaDashboardsUrl}/${traefikDashboardId}${options}`;

  registerResourceTableColumnsProcessor(function ageRemover({ id, columns }) {
    if (columns) {
      for (const element of columns) {
        if (element.label === "Grafana") {
          // Column has already been added
          return columns;
        }
      }
    }

    if (id === "headlamp-pods") {
      columns.push({
        label: "Grafana",
        getValue: (pod: Pod) => {
          const namespaceName = pod?.metadata?.namespace ?? "";
          const podName = pod?.metadata?.name ?? "";
          const openUrl = `${podDashboardUrl}&var-namespace=${namespaceName}&var-pod=${podName}`;
          return (
            <IconButton onClick={() => window.open(openUrl)} size="large">
              <Icon icon={"mdi:open-in-new"} />
            </IconButton>
          );
        },
        gridTemplate: 0.2,
      });
    } else if (id === "headlamp-namespaces") {
      columns.push({
        label: "Grafana",
        getValue: (namespace: Namespace) => {
          const namespaceName = namespace?.jsonData?.metadata?.name ?? "";
          const openUrl = `${namespaceDashboardUrl}&var-namespace=${namespaceName}`;
          return (
            <IconButton onClick={() => window.open(openUrl)} size="large">
              <Icon icon={"mdi:open-in-new"} />
            </IconButton>
          );
        },
        gridTemplate: 0.2,
      });
    } else if (id === "headlamp-nodes") {
      columns.push({
        label: "Grafana",
        getValue: (node: Node) => {
          const nodeName = node?.jsonData?.metadata?.name ?? "";
          const openUrl = `${nodeDashboardUrl}&var-node=${nodeName}`;
          return (
            <IconButton onClick={() => window.open(openUrl)} size="large">
              <Icon icon={"mdi:open-in-new"} />
            </IconButton>
          );
        },
        gridTemplate: 0.2,
      });
    } else if (id === "headlamp-services") {
      columns.push({
        label: "Grafana",
        getValue: (service: Service) => {
          const serviceName = service?.jsonData?.metadata?.name ?? "";
          const namespaceName = service?.jsonData?.metadata?.namespace ?? "";
          const port = service?.jsonData?.spec?.ports[0]?.port ?? "";
          const openUrl = `${traefikDashboardUrl}&var-service=${namespaceName}-${serviceName}-${port}`;
          return (
            <IconButton onClick={() => window.open(openUrl)} size="large">
              <Icon icon={"mdi:open-in-new"} />
            </IconButton>
          );
        },
        gridTemplate: 0.2,
      });
    } else if (id === "headlamp-ingresses") {
      columns.push({
        label: "Grafana",
        getValue: (ingress: Ingress) => {
          const serviceName =
            ingress?.jsonData?.spec?.rules[0]?.http?.paths[0]?.backend?.service
              ?.name ?? "";
          const namespaceName = ingress?.jsonData?.metadata?.namespace ?? "";
          const port =
            ingress?.jsonData?.spec?.rules[0]?.http?.paths[0]?.backend?.service
              ?.port?.number ?? "";
          const openUrl = `${traefikDashboardUrl}&var-service=${namespaceName}-${serviceName}-${port}`;
          return (
            <IconButton onClick={() => window.open(openUrl)} size="large">
              <Icon icon={"mdi:open-in-new"} />
            </IconButton>
          );
        },
        gridTemplate: 0.2,
      });
    }

    return columns;
  });
}
