import {
  DefaultHeadlampEvents,
  HeadlampEvent,
  registerAppLogo,
  registerHeadlampEventCallback,
} from '@kinvolk/headlamp-plugin/lib';
import applyClusterChooser from './components/cluster-chooser';
import applyContainerCustomizations from './components/containers';
import applyGrafanaLinks from './components/grafana-links';
import applyHelpButton from './components/help';
import applyIngressCustomizations from './components/ingresses';
import SimpleLogo from './components/logo';
import applyNodepoolCustomizations from './components/nodepools';
import applySplunkLinks from './components/splunk-links';

// Register components that do not require namespaces to be defined.
registerAppLogo(SimpleLogo);
applyHelpButton();
applyClusterChooser();

registerHeadlampEventCallback((event: HeadlampEvent) => {
  /*
  // This is to print all events to the console
  if (event.type === DefaultHeadlampEvents.ERROR_BOUNDARY) {
    console.error('Error:', event.data);
  } else {
    console.log(`Headlamp event of type ${event.type}:`, event.data);
  }
  */
  if (event.type === DefaultHeadlampEvents.PLUGINS_LOADED) {
    applyContainerCustomizations();
    applyIngressCustomizations();
    applyNodepoolCustomizations();
    applyGrafanaLinks();
    applySplunkLinks();
  }
});
