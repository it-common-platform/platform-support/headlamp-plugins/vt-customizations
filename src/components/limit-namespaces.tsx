// ChatGPT 3.5 used for getNamespaces()
import { getNamespace, listNamespaces } from "../api/limit-namespaces";

export default async function limitNamespaces(cluster: string) {
  const results = await getNamespaces();
  const defaults = { defaultNamespace: "", allowedNamespaces: [] };

  if (results?.length) {
    const currentString = localStorage.getItem(`cluster_settings.${cluster}`);
    const current = JSON.parse(currentString) || {};

    // Add default empty string when not set
    if (!current?.defaultNamespace) {
      current.defaultNamespace = defaults.defaultNamespace;
    }

    // Add default empty array when not set
    if (!current?.allowedNamespaces) {
      current.allowedNamespaces = defaults.allowedNamespaces;
    }

    // Populate a map to lookup new namespaces against those that already exist.
    const keyValues: [string, string][] = results.map(
      (value: string): [string, string] => [value, value]
    );
    const namespaces = new Map<string, string>(keyValues);
    let reload = false;

    for (const namespace of current?.allowedNamespaces) {
      if (!namespaces.get(namespace)) {
        reload = true;
      }
    }

    // Only set and reload if there has been a change in the length of the localStore namespaces
    if (!(results.length === current?.allowedNamespaces?.length) || reload) {
      const updated = currentString
        ? { ...current, allowedNamespaces: results }
        : { defaultNamespace: "", allowedNamespaces: results };
      const updatedString = JSON.stringify(updated);

      localStorage.setItem(`cluster_settings.${cluster}`, updatedString);
      window.location.reload();
    }
  } else {
    // Prevent the load from overwriting admin namespaces if they are set manually
    const currentString = localStorage.getItem(`cluster_settings.${cluster}`);

    if (!currentString) {
      // Set defaults in the case that there are not allowedNamespace or admin permissions.
      const defaultString = JSON.stringify(defaults);

      localStorage.setItem(`cluster_settings.${cluster}`, defaultString);
    }
  }
}

/**
 * Fetches all namespaces and evaluates a users allowed namespaces.
 * Errors are ignored as they are as a result of a permission error.
 */
async function getNamespaces(): Promise<string[] | []> {
  const allowedNamespaces = [];

  try {
    const allNamespaces = await listNamespaces();

    // .allSettled prevents from throwing if a single call produces an error.
    // .all will throw if any one promise throws.
    await Promise.allSettled(
      allNamespaces.items?.map(async (namespace: any) => {
        const name = namespace.metadata.name;

        try {
          // If this results in a 200 then we know the user has access
          // otherwise if it throws we know they do not.
          await getNamespace(name);

          allowedNamespaces.push(name);
        } catch (error) {
          if (!(error.status === 403 || error.status === 401)) {
            console.error("Unable to evaluate user namespaces", error.stack);
          }
        }
      })
    );

    if (allowedNamespaces.length >= allNamespaces.items.length) {
      return [];
    }

    return allowedNamespaces;
  } catch (error) {
    // We need to log this as it will not be clear to users if this occurs.
    // However, we're making the assumption that the error is a 401 from the namespaces lookup.
    if (!(error.status === 403 || error.status === 401)) {
      console.error("Unable to evaluate user namespaces", error.stack);
    }
    // Under the assumption it is a permission error we return an empty namespace set.
    return allowedNamespaces;
  }
}
