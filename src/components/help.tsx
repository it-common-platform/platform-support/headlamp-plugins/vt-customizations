import { Icon } from "@iconify/react";
import { registerAppBarAction } from "@kinvolk/headlamp-plugin/lib";
import IconButton from "@mui/material/IconButton";

export default function applyHelpButton() {
  function helpButton() {
    return (
      <IconButton
        onClick={() => window.open("https://docs.platform.it.vt.edu")}
        size="large"
      >
        <Icon icon={"mdi:help-circle"} />
      </IconButton>
    );
  }

  registerAppBarAction(helpButton);
}
