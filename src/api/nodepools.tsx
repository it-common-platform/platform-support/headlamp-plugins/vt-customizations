import { ApiProxy } from "@kinvolk/headlamp-plugin/lib";

const request = ApiProxy.request;
export const getHeadlampAPIHeaders = () => ({
  "X-HEADLAMP_BACKEND-TOKEN": new URLSearchParams(window.location.search).get(
    "backendToken"
  ),
});

export function listNodepools() {
  return request(`/apis/karpenter.sh/v1beta1/nodepools`, {
    method: "GET",
    headers: { ...getHeadlampAPIHeaders() },
  });
}
