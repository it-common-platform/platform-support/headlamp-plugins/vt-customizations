import { registerClusterChooser } from "@kinvolk/headlamp-plugin/lib";
import { useClustersConf } from "@kinvolk/headlamp-plugin/lib/k8s";
import { FormControl, InputLabel, MenuItem } from "@mui/material";
import Select from "@mui/material/Select";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import limitNamespaces from "./limit-namespaces";

const CHOICES = [
  { name: "AWS PROD", server: "https://headlamp.prod.aws.itcp.cloud.vt.edu/" },
  { name: "AWS PPRD", server: "https://headlamp.pprd.aws.itcp.cloud.vt.edu/" },
  { name: "AWS DVLP", server: "https://headlamp.dvlp.aws.itcp.cloud.vt.edu/" },
  {
    name: "On-prem PROD",
    server: "https://headlamp.prod.op.itcp.cloud.vt.edu/",
  },
  {
    name: "On-prem PPRD",
    server: "https://headlamp.pprd.op.itcp.cloud.vt.edu/",
  },
  {
    name: "On-prem DVLP",
    server: "https://headlamp.dvlp.op.itcp.cloud.vt.edu/",
  },
];

const getSortValue = (cluster: string, env: string) =>
  // Create a numerical value to represent the sort order. AWS first then prod -> dev
  (env === "prod" ? 1 : env === "pprd" ? 2 : 3) + (cluster === "aws" ? 1 : 10);
const sortClusters = (clusters) =>
  clusters
    .map(({ name }) => ({ name, server: name }))
    .sort((a: { name: string }, b: { name: string }) => {
      // eslint-disable-next-line no-unused-vars
      const [_, aCluster, aEnv] = a.name.split("-");
      // eslint-disable-next-line no-unused-vars
      const [__, bCluster, bEnv] = b.name.split("-");

      return getSortValue(aCluster, aEnv) - getSortValue(bCluster, bEnv);
    });

function ClusterChooser(props: any) {
  const { defaultValue, clusters, onChange } = props;

  return (
    <FormControl fullWidth sx={{ m: 1, minWidth: "120px" }}>
      <InputLabel>Choose a Cluster</InputLabel>
      <Select label="Cluster" defaultValue={defaultValue} onChange={onChange}>
        {clusters.map(({ name, server }, key: number) => (
          <MenuItem key={key} value={server}>
            {name}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}

export default function applyClusterChooser() {
  registerClusterChooser((options: any, props: any) => {
    const clusters = useClustersConf();
    // The cluster config when running in the cluster will return main as a cluster.
    // Though we cannot stop a developer or user from registering a cluster using that alias.
    const history = useHistory();
    const [current] = useState(history.location.pathname);
    const [isProd] = useState(
      clusters?.main && clusters?.main?.meta_data?.source === "incluster"
    );
    const [currentCluster, setCurrentCluster] = useState<string | null>();

    const handleChange = (event) => {
      const value = event?.target?.value;
      if (isProd) {
        window.location.replace(`${value}/c/main`);
      } else {
        history.push(`/c/${value}/`);
        // Reload to fetch updated data.
        window.location.reload();
      }
    };

    // When the component renders check to see if there is a cluster in the URI and set the cluster
    useEffect(() => {
      // eslint-disable-next-line no-unused-vars
      const [_, c, cluster] = current.split("/");

      // When the path contains a cluster then we should set the current cluster
      if (c === "c" && cluster) {
        setCurrentCluster(cluster);
      }
    }, []);

    // If the namespace changes then check the users allowed namespaces.
    useEffect(() => {
      if (currentCluster) {
        limitNamespaces(currentCluster);
      }
    }, [currentCluster]);

    return (
      <ClusterChooser
        {...options}
        {...props}
        clusters={isProd ? CHOICES : sortClusters(Object.values(clusters))}
        onChange={handleChange}
        defaultValue={isProd ? `${window.location.origin}/` : options.cluster}
      />
    );
  });
}
