import { Icon } from "@iconify/react";
import { registerResourceTableColumnsProcessor } from "@kinvolk/headlamp-plugin/lib";
import Namespace from "@kinvolk/headlamp-plugin/lib/k8s/namespace";
import Node from "@kinvolk/headlamp-plugin/lib/k8s/node";
import Pod from "@kinvolk/headlamp-plugin/lib/k8s/pod";
import DaemonSet from "@kinvolk/headlamp-plugin/lib/lib/k8s/daemonSet";
import ReplicaSet from "@kinvolk/headlamp-plugin/lib/lib/k8s/replicaSet";
import StatefulSet from "@kinvolk/headlamp-plugin/lib/lib/k8s/statefulSet";
import { IconButton } from "@mui/material";

export default function applySplunkLinks() {
  let url = "https://headlamp.dvlp.op.itcp.cloud.vt.edu/c/main";
  if (window.location.href.startsWith("https://")) {
    url = window.location.href;
  }

  const baseUrl = url?.split("/")[2] ?? "";
  const location = baseUrl.split(".")[2].replace("op", "eksa") ?? "undefined";
  const tier = baseUrl.split(".")[1] ?? "undefined";
  const clusterName = `platform-${location}-${tier}`;

  const splunkIndex = "*";
  const splunkBaseUrl = `https://splunk.it.vt.edu/en-US/app/it/search?q=index%3D${splunkIndex}%20%22kubernetes.clustername%22%3D%22${clusterName}%22`;

  registerResourceTableColumnsProcessor(function ageRemover({ id, columns }) {
    if (columns) {
      for (const element of columns) {
        if (element.label === "Splunk") {
          // Column has already been added
          return columns;
        }
      }
    }

    if (id === "headlamp-namespaces") {
      columns.push({
        label: "Splunk",
        getValue: (namespace: Namespace) => {
          const namespaceName = namespace?.jsonData?.metadata?.name ?? "";
          const openUrl = `${splunkBaseUrl}%20%22kubernetes.namespace%22%3D%22${namespaceName}%22`;
          return (
            <IconButton onClick={() => window.open(openUrl)} size="large">
              <Icon icon={"mdi:open-in-new"} />
            </IconButton>
          );
        },
        gridTemplate: 0.2,
      });
    } else if (id === "headlamp-nodes") {
      columns.push({
        label: "Splunk",
        getValue: (node: Node) => {
          const nodeName = node?.jsonData?.metadata?.name ?? "";
          const openUrl = `${splunkBaseUrl}%20%22kubernetes.node.name%22%3D%22${nodeName}%22`;
          return (
            <IconButton onClick={() => window.open(openUrl)} size="large">
              <Icon icon={"mdi:open-in-new"} />
            </IconButton>
          );
        },
        gridTemplate: 0.2,
      });
    } else if (id === "headlamp-pods") {
      columns.push({
        label: "Splunk",
        getValue: (pod: Pod) => {
          const namespaceName = pod?.metadata?.namespace ?? "";
          const podName = pod?.metadata?.name ?? "";
          const openUrl = `${splunkBaseUrl}%20%22kubernetes.namespace%22%3D%22${namespaceName}%22%20%22kubernetes.pod.name%22%3D%22${podName}%22`;
          return (
            <IconButton onClick={() => window.open(openUrl)} size="large">
              <Icon icon={"mdi:open-in-new"} />
            </IconButton>
          );
        },
        gridTemplate: 0.2,
      });
    } else if (id === "headlamp-statefulsets") {
      columns.push({
        label: "Splunk",
        getValue: (statefulSet: StatefulSet) => {
          const namespaceName = statefulSet?.metadata?.namespace ?? "";
          const statefulSetName = statefulSet?.jsonData?.metadata?.name ?? "";
          const openUrl = `${splunkBaseUrl}%20%22kubernetes.namespace%22%3D%22${namespaceName}%22%20%22kubernetes.statefulset.name%22%3D%22${statefulSetName}%22`;
          return (
            <IconButton onClick={() => window.open(openUrl)} size="large">
              <Icon icon={"mdi:open-in-new"} />
            </IconButton>
          );
        },
        gridTemplate: 0.2,
      });
    } else if (id === "headlamp-daemonsets") {
      columns.push({
        label: "Splunk",
        getValue: (daemonSet: DaemonSet) => {
          const namespaceName = daemonSet?.metadata?.namespace ?? "";
          const daemonSetName = daemonSet?.jsonData?.metadata?.name ?? "";
          const openUrl = `${splunkBaseUrl}%20%22kubernetes.namespace%22%3D%22${namespaceName}%22%20%22kubernetes.daemonset.name%22%3D%22${daemonSetName}%22`;
          return (
            <IconButton onClick={() => window.open(openUrl)} size="large">
              <Icon icon={"mdi:open-in-new"} />
            </IconButton>
          );
        },
        gridTemplate: 0.2,
      });
    } else if (id === "headlamp-replicasets") {
      columns.push({
        label: "Splunk",
        getValue: (replicaSet: ReplicaSet) => {
          const namespaceName = replicaSet?.metadata?.namespace ?? "";
          const replicaSetName = replicaSet?.jsonData?.metadata?.name ?? "";
          const openUrl = `${splunkBaseUrl}%20%22kubernetes.namespace%22%3D%22${namespaceName}%22%20%22kubernetes.replicaset.name%22%3D%22${replicaSetName}%22`;
          return (
            <IconButton onClick={() => window.open(openUrl)} size="large">
              <Icon icon={"mdi:open-in-new"} />
            </IconButton>
          );
        },
        gridTemplate: 0.2,
      });
    }

    return columns;
  });
}
