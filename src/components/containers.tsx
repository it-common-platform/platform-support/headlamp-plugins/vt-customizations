import {
  DefaultDetailsViewSection,
  registerDetailsViewSectionsProcessor,
} from "@kinvolk/headlamp-plugin/lib";
import {
  SectionBox,
  SimpleTable,
} from "@kinvolk/headlamp-plugin/lib/CommonComponents";
import { KubeObject } from "@kinvolk/headlamp-plugin/lib/k8s/cluster";
import { useEffect, useState } from "react";
import { listPods } from "../api/containers";

export interface ContainerListProps {
  resource?: KubeObject;
}

export default function applyContainerCustomizations() {
  registerDetailsViewSectionsProcessor();
}

function ContainerList(props: ContainerListProps) {
  const { resource } = props;
  const [containers, setContainers] = useState<Array<any>>([]);
  const [initContainers, setInitContainers] = useState<Array<any>>([]);

  const namespace = resource?.jsonData?.metadata?.name ?? "";

  useEffect(() => {
    if (!namespace) {
      // If namespace is not available, return early
      setContainers([]);
      setInitContainers([]);
      return;
    }

    listPods(namespace).then((response) => {
      if (!response?.items) {
        setContainers([]);
        setInitContainers([]);
        return;
      }
      response.items.forEach((pod) => {
        if (pod.spec.containers) {
          pod.spec.containers.forEach((container) => {
            containers.push(container);
          });
        }
        if (pod.spec.initContainers) {
          pod.spec.initContainers.forEach((initContainer) => {
            initContainers.push(initContainer);
          });
        }
      });
    });
  }, []);

  return (
    <>
      <SectionBox title="Containers" textAlign="center" paddingTop={2}>
        <SimpleTable
          columns={[
            {
              label: "Name",
              getter: (container) => container?.name ?? "",
            },
            {
              label: "Image",
              getter: (container) => container?.image ?? "",
            },
            {
              label: "Volumes",
              getter: (container) =>
                getVolumeInfo(container?.volumeMounts ?? []) ?? "",
            },
          ]}
          data={containers}
        />
      </SectionBox>
      <SectionBox title="Init Containers" textAlign="center" paddingTop={2}>
        <SimpleTable
          columns={[
            {
              label: "Name",
              getter: (initContainer) => initContainer?.name ?? "",
            },
            {
              label: "Image",
              getter: (initContainer) => initContainer?.image ?? "",
            },
            {
              label: "Volumes",
              getter: (initContainer) =>
                getVolumeInfo(initContainer?.volumeMounts ?? []) ?? "",
            },
          ]}
          data={initContainers}
        />
      </SectionBox>
    </>
  );
}

function getVolumeInfo(volumes) {
  let volumeString = "";
  volumes.forEach(function (volume, index, array) {
    const stringToAdd = `${volume.name ?? ""} is mounted at ${
      volume.mountPath ?? ""
    }`;
    volumeString = volumeString.concat(stringToAdd);
    if (index !== array.length - 1) {
      volumeString = volumeString.concat(" | ");
    }
  });
  return volumeString;
}

registerDetailsViewSectionsProcessor(function addSubheaderSection(
  resource,
  sections
) {
  // Ignore if there is no resource.
  if (!resource) {
    return sections;
  }

  if (resource.kind !== "Namespace") {
    // Return early if we're not on a namespace page
    return sections;
  }

  // Check if we already have added our custom section (this function may be called multiple times).
  const customSectionId = "add-containers-to-namespace";
  if (sections.findIndex((section) => section.id === customSectionId) !== -1) {
    return sections;
  }

  const detailsHeaderIdx = sections.findIndex(
    (section) => section.id === DefaultDetailsViewSection.MAIN_HEADER
  );
  // There is no header, so we do nothing.
  if (detailsHeaderIdx === -1) {
    return sections;
  }

  // We place our custom section after the header.
  sections.splice(detailsHeaderIdx + 7, 0, {
    id: "add-containers-to-namespace",
    section: <ContainerList resource={resource} />,
  });

  return sections;
});
